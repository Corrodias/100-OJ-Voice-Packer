package corrodias.oj100;

import com.google.common.io.LittleEndianDataInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class Utils {

	public static String getString(LittleEndianDataInputStream in) throws IOException {
		byte[] buf = new byte[255];
		byte b;
		int i = 0;
		while ((b = in.readByte()) != 0) {
			buf[i] = b;
			i++;
		}
		return new String(buf, 0, i, StandardCharsets.US_ASCII);
	}

	public static void copyStream(InputStream in, OutputStream out, long length) throws IOException {
		byte[] buffer = new byte[8192];
		int read = in.read(buffer, 0, (int) Math.min(buffer.length, length));
		length -= read;
		while (read != -1) {
			out.write(buffer, 0, read);
			if (length == 0)
				return;
			read = in.read(buffer, 0, (int) Math.min(buffer.length, length));
			length -= read;
		}
	}
}
