package corrodias.oj100;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class Unpacker {
	File input;

	public Unpacker(Path input) {
		this.input = input.toFile();
	}

	public void unpack(Path outputDirectory) throws Exception {
		if (input == null || outputDirectory == null)
			return;
		try (LittleEndianDataInputStream in = new LittleEndianDataInputStream(new BufferedInputStream(new FileInputStream(input)))) {
			Files.createDirectory(outputDirectory);

			//noinspection InfiniteLoopStatement
			while (true) {
				long dataLength = in.readInt() & 0xFFFFFFFFL;
				String fileName = Utils.getString(in);

				int formatLength = in.readInt();
				byte[] formatTag = new byte[formatLength];
				//noinspection ResultOfMethodCallIgnored
				in.read(formatTag);

				System.out.println("Writing " + fileName);
				try (LittleEndianDataOutputStream out = new LittleEndianDataOutputStream(new BufferedOutputStream(Files.newOutputStream(outputDirectory.resolve(fileName))))) {
					// header
					out.write("RIFF".getBytes(StandardCharsets.US_ASCII));
					// total file size - 8;
					// "WAVE" + "fmt " + format tag length (4) + format tag + "data" + data tag length (4) + data tag
					out.writeInt((int) (4 + 4 + 4 + formatLength + 4 + 4 + dataLength));
					out.write("WAVE".getBytes(StandardCharsets.US_ASCII));
					// fmt tag
					out.write("fmt ".getBytes(StandardCharsets.US_ASCII));
					out.writeInt(formatLength);
					out.write(formatTag);
					// data tag
					out.write("data".getBytes(StandardCharsets.US_ASCII));
					out.writeInt((int) dataLength);
					Utils.copyStream(in, out, dataLength);
				}
			}
		} catch (EOFException ignored) {
			// end of the file
		}
	}
}
