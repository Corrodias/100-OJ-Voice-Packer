package corrodias.oj100;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class Packer {
	Path input;

	public Packer(Path input) {
		this.input = input;
	}

	public void pack(Path outputFile) throws Exception {
		if (input == null || outputFile == null)
			return;
		try (LittleEndianDataOutputStream out = new LittleEndianDataOutputStream(new BufferedOutputStream(Files.newOutputStream(outputFile, StandardOpenOption.CREATE_NEW)))) {
			Files.walkFileTree(input, new PackingFileVisitor(out));
		}
	}

	private static class PackingFileVisitor extends SimpleFileVisitor<Path> {
		LittleEndianDataOutputStream out;

		public PackingFileVisitor(LittleEndianDataOutputStream out) {
			this.out = out;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attr) throws IOException {
			if (out == null)
				return FileVisitResult.TERMINATE;
			System.out.println("Reading " + file.toFile().getName());
			packFile(file);
			return FileVisitResult.CONTINUE;
		}

		private void packFile(Path file) throws IOException {
			try (LittleEndianDataInputStream in = new LittleEndianDataInputStream(new BufferedInputStream(Files.newInputStream(file)))) {
				byte[] nameBuf = new byte[4];
				in.read(nameBuf);
				if (!"RIFF".equals(new String(nameBuf, StandardCharsets.US_ASCII)))
					throw new IOException("The file is not a valid RIFF file.");
				in.readInt(); // discard the file length
				in.read(nameBuf);
				if (!"WAVE".equals(new String(nameBuf, StandardCharsets.US_ASCII)))
					throw new IOException("The file is not a valid RIFF WAVE file.");

				int formatLength = 0;
				byte[] formatTag = null;

				//noinspection InfiniteLoopStatement
				while (true) {
					in.read(nameBuf);
					String tagName = new String(nameBuf, StandardCharsets.US_ASCII);
					if ("fmt ".equals(tagName)) {
						formatLength = in.readInt();
						formatTag = new byte[formatLength];
						//noinspection ResultOfMethodCallIgnored
						in.read(formatTag);
					} else if ("data".equals(tagName)) {
						if (formatTag == null)
							throw new IOException("A fmt tag did not precede the data tag.");

						long dataLength = in.readInt() & 0xFFFFFFFFL;
						out.writeInt((int) dataLength); // data length
						out.write(file.toFile().getName().getBytes(StandardCharsets.US_ASCII));
						out.writeByte(0);
						out.writeInt(formatLength);
						out.write(formatTag);
						Utils.copyStream(in, out, dataLength);
						return;
					}
				}
			} catch (EOFException ignored) {
				throw new IOException("The input file is incomplete.");
			}
		}
	}
}
