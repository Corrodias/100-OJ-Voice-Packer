package corrodias.oj100;

import com.google.common.io.LittleEndianDataInputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

	public static void main(String[] args) throws Exception {
		System.out.println("Current directory: " + Paths.get(".").toAbsolutePath());

		if (args.length == 0) {
			System.out.println("You must specify a file to unpack or a directory to pack.");
			return;
		}

		Path input = Paths.get(args[0]);
		System.out.println("Processing " + input.toAbsolutePath());

		if (input.toFile().isFile()) {
			System.out.println("Unpacking " + args[0]);
			new Unpacker(input).unpack(getPathWithoutExtension(input));
		} else if (input.toFile().isDirectory()) {
			System.out.println("Packing " + args[0]);
			new Packer(input).pack(getPathWithAddedExtension(input));
		} else {
			System.out.println("File not found or inaccessible.");
		}
		System.out.println("Done.");
	}

	private static Path getPathWithoutExtension(Path input) {
		if (input == null)
			return null;
		String str = input.toFile().getName();
		int pos = str.lastIndexOf(".");
		if (pos == -1)
			return input;
		return Paths.get(str.substring(0, pos));
	}

	private static Path getPathWithAddedExtension(Path input) {
		if (input == null)
			return null;
		String str = input.toFile().getName();
		return Paths.get(str + ".pak");
	}
}
